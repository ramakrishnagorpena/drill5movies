const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 


// Find all the movies with total earnings more than $500M

const earnings = Object.keys(favouritesMovies).reduce((accumulator, favmovies) =>

     {
        if(Number(favouritesMovies[favmovies].totalEarnings.slice(1, -1))>500){
            accumulator[favmovies] = favouritesMovies[favmovies];
        }
        return accumulator;

    },{})

// console.log(earnings);


// Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

const oscarNominations = Object.keys(favouritesMovies).reduce((accumulator, favmovies) =>

     {
        if(favouritesMovies[favmovies]. oscarNominations>3 && Number(favouritesMovies[favmovies].totalEarnings.slice(1, -1))>500){
            accumulator[favmovies] = favouritesMovies[favmovies];
        }
        return accumulator;

    },{})

// console.log(oscarNominations);


// Find all movies of the actor "Leonardo Dicaprio".

const actor = Object.keys(favouritesMovies).reduce((accumulator, favmovies) =>

     {
        if(favouritesMovies[favmovies]. actors.includes("Leonardo Dicaprio") ){
            accumulator[favmovies] = favouritesMovies[favmovies];
        }
        return accumulator;

},{})

// console.log(actor);


// Sort movies (based on IMDB rating)
//         if IMDB ratings are same, compare totalEarning as the secondary metric.

let sort = Object.entries(favouritesMovies).sort((a,b)=>{
    if(a[1].imdbRating - b[1].imdbRating != 0){
        return a[1].imdbRating - b[1].imdbRating;
    }else{
        let aTotalEarnings = Number(a[1].totalEarnings.slice(1, -1));
        let bTotalEarnings = Number(b[1].totalEarnings.slice(1, -1));
        return aTotalEarnings - bTotalEarnings;
    }
})
// console.log(sort);


// Group movies based on genre. Priority of genres in case of multiple genres present are:
//         drama > sci-fi > adventure > thriller > crime

// const groupMovies =